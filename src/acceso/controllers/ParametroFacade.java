/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acceso.controllers;

import acceso.definiciones.Parametro;

/**
 *
 * @author rcarlos
 */
class ParametroFacade extends Facade<Parametro>{
    
    private ParametroFacade() {
        super(Parametro.class);
    }
    
}
