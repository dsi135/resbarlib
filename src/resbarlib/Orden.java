/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resbarlib;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author josesvx
 */
public class Orden {

    public int idOrden;
    public String mesero;
    public String mesa;
    public Date fecha;
    public Double total;
    public boolean activa;
    public DetalleOrden[] detalle;

    public Double CalcularTotal() {
        double acumulador = 0;
        for (DetalleOrden detalleOrden : detalle) {
            acumulador = acumulador + (detalleOrden.producto.precio * detalleOrden.cantidad);
        }
        total = acumulador;
        return total;
    }

    public void AgregarProducto() {
        if (detalle == null){
            
        }
    }

    public void EliminarProducto() {
    }

}
